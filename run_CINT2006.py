import datetime
import os
import json
import subprocess
import sys

def build_benchmark(benchmark, config):
    """
    Builds a given benchmark for later execution."
    """
    print("Building benchmark '{}'".format(benchmark))
    cmd = ["runspec", "--config", config["config"], "--action", "build",
           "--tune", config["tune"], benchmark]
    subprocess.call(cmd)

def make_benchmark_run_script(benchmark, config):
    """
    Creates a temporary script for running one of SPEC's benchmarks.
    Returns the path to the generated script file.
    """
    fpath = os.path.join(os.sep + "tmp", benchmark + ".tmp.sh")
    if not os.path.exists(fpath):
        print("Creating script to run benchmark '{}'".format(benchmark))
        with open(fpath, "wt") as f:
            cmd = "runspec --noreportable --config={} --tune={} {}"
            f.write(cmd.format(config["config"], config["tune"], benchmark))
    else:
        print("Script to run benchmark '{}' already exists".format(benchmark))
    return fpath

def run_benchmark(benchmark_script, config):
    """
    Executes and measures the running time of given benchmark.
    """
    print("Running benchmark '{}'".format(benchmark_name))
    benchmark_name = benchmark_script.split(os.sep)[2].split(".")[0]

    start_time = datetime.datetime.now()
    subprocess.call([config["qemu_path"], "-sh", config["shepherd_path"], \
                     benchmark_script])
    end_time = datetime.datetime.now()

    return end_time - start_time

if __name__ == "__main__":
    config_file = "config.json"

    # Load configuration file.
    try:
        with open(config_file, "rt") as f:
            config = json.load(f)
            os.changedir(config["spec"]["directory"])
            times_to_run = config["spec"]["times_to_run"]
            results = {}
            for benchmark in config["spec"]["benchmarks"]:
                #build_benchmark(benchmark, config["spec"])
                benchmark_script = make_benchmark_run_script(benchmark, \
                                                         config["spec"])

                results[benchmark] = [run_benchmark(benchmark_script, config) \
                                      for i in xrange(times_to_run)]
    except IOError:
        sys.stderr.write("Error: missing config file '{}'".format(config_file))
