SPEC scripts
============

Este repositório contém os arquivos necessários para execução dos bechmarks do
SPEC CPU (Int) 2006.

### Organização de arquivos

Abaixo encontram-se as descrições dos arquivos e diretórios mais relevantes:

+ `config.json`: Arquivo com as configurações necessárias para compilação e
execução dos benchmarks.
+ `run_CINT2006.py`: Script responsável pela execução e coleta de dados
(tempo de execução, etc) dos benchmarks.
+ `venv`: Diretório de um ambiente virtual com as bibliotecas (de Python)
necessárias para execução dos testes e coleta de resultados.

### Como utilizar

Antes de iniciar a execução dos testes, é necessário ativar o ambiente virtual
para o Python:

`$ source venv/bin/activate`

Após modificar o arquivo `config.json` de forma a atender às suas necessidades,
utilize o comando abaixo para testar os benchmarks:

`$ python run_CINT2006.py`

Quando quiser desativar o ambiente virtual:

`$ deactivate`
